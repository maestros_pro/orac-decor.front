export default class Share {
	constructor(options) {

		this._polyfill();

		Object.assign(this._options = {}, this._default(), options, this._social());

		if (document.readyState === 'loading') {
			document.addEventListener('DOMContentLoaded', () => {
				this._init();
			});
		} else {
			this._init();
		}
	}

	_polyfill() {

		//assign
		if (typeof Object.assign != 'function') {
			Object.assign = function (target, varArgs) { // .length of function is 2
				'use strict';
				if (target == null) { // TypeError if undefined or null
					throw new TypeError('Cannot convert undefined or null to object');
				}

				var to = Object(target);

				for (var index = 1; index < arguments.length; index++) {
					var nextSource = arguments[index];

					if (nextSource != null) { // Skip over if undefined or null
						for (var nextKey in nextSource) {
							// Avoid bugs when hasOwnProperty is shadowed
							if (Object.prototype.hasOwnProperty.call(nextSource, nextKey)) {
								to[nextKey] = nextSource[nextKey];
							}
						}
					}
				}
				return to;
			};
		}

		if (!Array.from) {
			Array.from = (function () {
				let toStr = Object.prototype.toString,
					isCallable = (fn) => typeof fn === 'function' || toStr.call(fn) === '[object Function]',
					toInteger = (value) => {
						let number = Number(value);
						if (isNaN(number)) {
							return 0;
						}
						if (number === 0 || !isFinite(number)) {
							return number;
						}
						return (number > 0 ? 1 : -1) * Math.floor(Math.abs(number));
					},
					maxSafeInteger = Math.pow(2, 53) - 1,
					toLength = function (value) {
						let len = toInteger(value);
						return Math.min(Math.max(len, 0), maxSafeInteger);
					};

				return function from(arrayLike) {
					let C = this,
						items = Object(arrayLike);
					if (arrayLike == null) throw new TypeError('Array.from requires an array-like object - not null or undefined');

					let mapFn = arguments[1];

					if (typeof mapFn !== 'undefined') {
						mapFn = arguments.length > 1 ? arguments[1] : void undefined;
						if (!isCallable(mapFn)) throw new TypeError('Array.from: when provided, the second argument must be a function');
						if (arguments.length > 2) T = arguments[2];
					}

					let len = toLength(items.length),
						A = isCallable(C) ? Object(new C(len)) : new Array(len),
						k = 0,
						kValue;

					while (k < len) {
						kValue = items[k];
						if (mapFn) {
							A[k] = typeof T === 'undefined' ? mapFn(kValue, k) : mapFn.call(T, kValue, k);
						} else {
							A[k] = kValue;
						}
						k += 1;
					}
					A.length = len;
					return A;
				};
			}());
		}
	}

	_default() {
		return {
			fbAppId: null,
			dataPostfix: 'share',
			loadCount: false,
			callbackPopupShareOpen: function (type) {
			},
			callbackPopupShareClosed: function (type) {
			},
			callbackCountLoaded: function (type) {
			},
			callbackError: function (code, err) {
			}
		}
	}

	_init() {

		let shareWrap = document.querySelectorAll(`[data-${this._options.dataPostfix}]`);

		Array.from(shareWrap).forEach((wrap, i) => {

			if (shareWrap[i].dataset.shareinitialized) return false;
			shareWrap[i].dataset.shareinitialized = true;

			let dataAll = this._collectShareData(wrap),
				shareBtn = wrap.querySelectorAll(`[data-${this._options.dataPostfix}-type]`);

			dataAll.url = dataAll.url || window.location.href;

			if (shareBtn.length < 0) return false;

			Array.from(shareBtn).forEach(btn => {
				let data = {};
				data.fbAppId = this._options.fbAppId;
				Object.assign(data, dataAll, this._collectShareData(btn));

				btn.addEventListener('click', (e) => {
					if (data.img && !data.img.match(/^https?:/g) && data.img.match(/^\//g)) data.img = window.location.origin + data.img;
					this._share(data, btn);
				});

				if (this._options.loadCount) {
					this.count(data.type, data.url, (val) => {
						const btn = wrap.querySelectorAll(`[data-${this._options.dataPostfix}-type=${data.type}]`);
						const counter = wrap.querySelectorAll(`[data-${this._options.dataPostfix}-count=${data.type}]`);
						if (val > 0) counter[0].innerHTML = val;

						if (this._options.callbackCountLoaded && typeof this._options.callbackCountLoaded === 'function') this._options.callbackCountLoaded({
							type: data.type,
							val
						}, btn[0]);
					})
				}
			});
			this._options.index++;
		});

		if (this._options.fbAppId) {
			this._getScript(this._options['fb'].apiUrl, () => {
				FB.init({
					appId: this._options.fbAppId,
					autoLogAppEvents: true,
					xfbml: true,
					version: 'v2.10'
				});
			});
		}
	}

	_social() {
		return {
			fb: {
				name: 'Facebook',
				counterUrl: 'https://graph.facebook.com/?id={url}',
				apiUrl: '//connect.facebook.net/en_US/sdk.js',
				shareUrl: (data) => {
					return (data && data.fbAppId) ?
						'https://www.facebook.com/dialog/feed?app_id={fbAppId}&redirect_uri={redirect_uri}&link={url}&title={title}&description={description}&picture={img}' :
						'https://www.facebook.com/sharer/sharer.php?u={url}&t={title}&picture={img}';
				},
				getCount: (data) => {
					try {
						return data.share.share_count;
					} catch (e) {
						return 0;
					}
				},
				popupDim: [600, 500]
			},
			vk: {
				name: 'ВКонтакте',
				counterUrl: 'https://vk.com/share.php?act=count&url={url}&index={index}',
				setCounter: () => {
					this._setObjectChain(window, 'VK.Share.count', (index, counter) => {
						this._options.vk.count = [];
						this._options.vk.count[this._options.index] = counter;
					});
				},
				getCount: (ind) => this._options.vk.count[ind],
				shareUrl: () => 'https://vk.com/share.php?url={url}&title={title}&description={description}&image={img}',
				popupDim: [550, 330]
			},
			ok: {
				name: 'Одноклассники',
				counterUrl: 'https://connect.ok.ru/dk?st.cmd=extLike&ref={url}&uid={index}',
				setCounter: () => {
					this._setObjectChain(window, 'ODKL.updateCount', (index, counter) => {
						this._options.ok.count = [];
						this._options.ok.count[this._options.index] = counter;
					});
				},
				getCount: (ind) => this._options.ok.count[ind],
				shareUrl: () => 'https://connect.ok.ru/dk?cmd=WidgetSharePreview&st.cmd=WidgetSharePreview&st._aid=ExternalShareWidget_SharePreview&st.imageUrl={img}&st.description={description}&st.shareUrl={url}&st.title={title}',
				popupDim: [640, 400]
			},
			gp: {
				name: 'Google+',
				// counterUrl: 'https://share.yandex.net/counter/gpp/?url={url}',
				// getCount: (data) => data,
				shareUrl: () => 'https://plus.google.com/share?url={url}',
				popupDim: [700, 500]
			},
			tw: {
				name: 'Twitter',
				counterUrl: null,
				shareUrl: () => 'https://twitter.com/intent/tweet?url={url}&text={title}&hashtags={hashtags}',
				popupDim: [600, 450]
			},
			mr: {
				name: 'mailru',
				counterUrl: null,
				shareUrl: () => 'https://connect.mail.ru/share?url={url}&title={title}&description={description}',
				popupDim: [530, 500]
			},
			index: 0
		}
	}

	_setObjectChain(object, key, value) {
		const frags = key.split('.');
		let last = null;

		frags.forEach((key, index) => {
			if (typeof object[key] === 'undefined') {
				object[key] = {};
			}

			if (index !== frags.length - 1) {
				object = object[key];
			}

			last = key;
		});

		object[last] = value;
	}

	_share(opts, btn) {

		let data = Object.assign({}, this._options, opts);

		if (data.fbAppId && data.type === 'fb') {
			let that = this;

			FB.ui({
				method: 'share_open_graph',
				action_type: 'og.shares',
				action_properties: JSON.stringify({
					object: {
						'og:url': data.url,
						'og:title': data.title,
						'og:description': data.description,
						'og:image:url': data.img
					}
				})
			}, function (response) {
				if (that._options.callbackPopupShareClosed && typeof that._options.callbackPopupShareClosed === 'function') that._options.callbackPopupShareClosed({type: data.type}, btn, response);
			});

		} else {
			this._openPopup(data, btn);
		}

	}

	_openPopup(data, btn) {

		const w = this._options[data.type].popupDim[0],
			h = this._options[data.type].popupDim[1],
			left = ( screen.width / 2 ) - ( w / 2 ),
			top = ( screen.height / 2 ) - ( h / 2 );

		const url = this._makeUrl(this._options[data.type].shareUrl(data), data);
		const win = window.open(url, data.type, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);

		if (this._options.callbackPopupShareOpen && typeof this._options.callbackPopupShareOpen === 'function') this._options.callbackPopupShareOpen({type: data.type}, btn);

		if (win) {
			const timer = setInterval(()=> {
				if (win.closed) {
					clearInterval(timer);
					if (this._options.callbackPopupShareClosed && typeof this._options.callbackPopupShareClosed === 'function') this._options.callbackPopupShareClosed({type: data.type}, btn);
				}
			}, 100);
		} else {
			if (this._options.callbackError && typeof this._options.callbackError === 'function') this._options.callbackError(1, 'Browser blocked popups');
		}

		return win;
	}

	_collectShareData(node) {
		let i,
			dataset = {},
			attributes = node.attributes,
			attribute,
			attributeName;

		for (i = attributes.length - 1; i >= 0; i--) {
			attribute = attributes[i];
			if (attribute && attribute.name && (/^data-\w[\w\-]*$/).test(attribute.name)) {
				attributeName = attribute.name.substr(5).replace(`${this._options.dataPostfix}-`, '');
				dataset[attributeName] = attribute.value;
			}
		}
		return dataset;
	}

	_makeUrl(text, data) {

		text = typeof text === 'function' ? text() : text;

		return text ? text.replace(/\{([^\}]+)\}/g, function (value, key) {
			return key in data ? encodeURIComponent(data[key]) : value;
		}).replace(/&([^\=]+)\=\{([^\}]+)\}/g, '') : '';
	}

	_getScript(url, callback) {

		if (!url) {
			if (callback) callback();
			return false;
		}

		let isLoaded = false, i = 0;

		for (i; i < document.getElementsByTagName('script').length; i++) {
			if (document.getElementsByTagName('script')[i].src === url) isLoaded = true;
		}

		if (!isLoaded) {
			let script = document.createElement('script'),
				prior = document.getElementsByTagName('script')[0];
			script.async = 1;
			script.onload = script.onreadystatechange = function (_, isAbort) {
				if (isAbort || !script.readyState || /loaded|complete/.test(script.readyState)) {
					script.onload = script.onreadystatechange = null;
					script = undefined;

					if (!isAbort) {
						if (callback) callback();
					}
				}
			};
			script.src = url;
			prior.parentNode.insertBefore(script, prior);
		} else if (callback) {
			callback();
		}

	}

	count(type, u, c) {

		if (!type) return console.error('Arguments are needed to count');

		let callback, page;

		if (u && typeof u === 'function') {
			callback = u;
			page = window.location.href;
		} else if (c && typeof c === 'function') {
			callback = c;
			page = u;
		}

		let url = this._makeUrl(this._options[type].counterUrl, {url: page});

		if (!url) return false;

		const setCount = (type, count) => {
			let val = +this._options[type].getCount(count);
			callback(val);
		};

		if (this._options[type].setCounter && typeof this._options[type].setCounter === 'function') {
			this._options[type].setCounter();
			this._getScript(url, () => {
				setCount(type, this._options.index);
			});
		} else {
			const callbackName = encodeURIComponent(`count_callback_share_${type}_${this._options.index}`);
			const path = `${url}&callback=${callbackName}`;
			this._getScript(path);
			window[callbackName] = (count) => {
				setCount(type, count);
			};
		}
	}

	share(type, options) {

		if (!type) return console.error('Arguments are needed to share');

		let data = options || {};

		if (typeof type === 'object') {
			data = type;
		} else if (typeof type === 'string') {
			data.type = type;
		}
		data.url = data.url || window.location.href;

		let opts = Object.assign({}, this._options, data);

		this._share(opts);

	}
}
