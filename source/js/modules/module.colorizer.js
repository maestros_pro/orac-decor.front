/**
 normal | multiply | screen | overlay |
 darken | lighten | color-dodge | color-burn | hard-light |
 soft-light | difference | exclusion | hue | saturation |
 color | luminosity
 */

export default class Colorizer {
	constructor(options) {

		this._polyfill();

		this.path = [];
		this.image = null;
		this.deltaPath = {
			scale: 1,
			x: 0,
			y: 0
		};
		this.blend = {
			path: [],
			mode: 'multiply',
			color: '#ffffff'
		};
		this.canvas = null;
		this.control = {};
		this.pick = '#ffffff';

		Object.assign(this._options = {}, this._default(), options);
		if (document.readyState === 'loading') {
			document.addEventListener('DOMContentLoaded', () => {
				this.init();
			});
		} else {
			this.init();
		}
	}

	_polyfill() {
		if (!Object.assign) {
			Object.defineProperty(Object, 'assign', {
				enumerable: false,
				configurable: true,
				writable: true,
				value: function (target, firstSource) {
					'use strict';
					if (target === undefined || target === null) {
						throw new TypeError('Cannot convert first argument to object');
					}

					let to = Object(target);
					for (let i = 1; i < arguments.length; i++) {
						let nextSource = arguments[i];
						if (nextSource === undefined || nextSource === null) {
							continue;
						}

						let keysArray = Object.keys(Object(nextSource));
						for (let nextIndex = 0, len = keysArray.length; nextIndex < len; nextIndex++) {
							let nextKey = keysArray[nextIndex],
								desc = Object.getOwnPropertyDescriptor(nextSource, nextKey);
							if (desc !== undefined && desc.enumerable) {
								to[nextKey] = nextSource[nextKey];
							}
						}
					}
					return to;
				}
			});
		}


		
	}

	_default() {
		return {
			element: false,
			paddings: [0, 0, 0, 0],
			control: false,

			controlOptions:{
				pickerCircleRadius: 10,
				pickerCirclePadding: 9,
				pickerCircleStroke: 2,
				pickerRectWidth: 8,
				lineWidth: 8,
				gradient: [
					'#f30610',
					'#e900ff',
					'#4c00ff',
					'#59ebfe',
					'#2dff00',
					'#f7ff03',
					'#e30a0c']
			}
		}
	}

	_buildControl(){
		let el = document.querySelectorAll(this._options.control)[0];

		this.control.canvas = document.createElement('canvas');
		this.control.canvas.width = el.clientWidth;
		this.control.canvas.height = el.clientHeight;
		el.appendChild(this.control.canvas);

		this._drawControl();
	}

	_drawControl(){

		let options = this._options.controlOptions;
		this.control.ctx = this.control.canvas.getContext('2d');

		let grd = this.control.ctx.createLinearGradient((this.control.canvas.width - options.lineWidth) / 2,(options.pickerCircleRadius + options.pickerCircleStroke) * 2 + options.pickerCirclePadding,options.lineWidth, this.control.canvas.height - ((options.pickerCircleRadius + options.pickerCircleStroke) * 2) + options.pickerCirclePadding);

		this.control.ctx.clearRect(0, 0, this.control.canvas.width, this.control.canvas.height);


		this.control.ctx.beginPath();
		this.control.ctx.fillStyle = this.pick;
		this.control.ctx.fillRect((this.control.canvas.width - options.pickerRectWidth) / 2,options.pickerCircleRadius + (options.pickerCircleStroke/ 2) - (options.pickerRectWidth / 2),options.pickerRectWidth,options.pickerRectWidth);

		this.control.ctx.beginPath();
		grd.addColorStop(0, this._options.controlOptions.gradient[0]);
		for(let i = 1; i < this._options.controlOptions.gradient.length; i++){
			grd.addColorStop((i * (1 / this._options.controlOptions.gradient.length)), this._options.controlOptions.gradient[i]);
		}
		this.control.ctx.fillStyle = grd;
		this.control.ctx.fillRect((this.control.canvas.width - options.lineWidth) / 2,(options.pickerCircleRadius + options.pickerCircleStroke) * 2 + options.pickerCirclePadding,options.lineWidth, this.control.canvas.height - ((options.pickerCircleRadius + options.pickerCircleStroke) * 2) + options.pickerCirclePadding);


		this.control.ctx.beginPath();
		this.control.ctx.arc(this.control.canvas.width / 2, options.pickerCircleRadius + (options.pickerCircleStroke / 2), options.pickerCircleRadius, 0, 2 * Math.PI);
		this.control.ctx.lineWidth = 2;
		this.control.ctx.strokeStyle = this.blend.color;
		this.control.ctx.stroke();

		window.requestAnimationFrame(this._drawControl.bind(this));
	}

	_buildCanvas(){
		this.canvas = document.createElement('canvas');
		this.canvas.width = this.el.clientWidth;
		this.canvas.height = this.el.clientHeight;
		this.el.appendChild(this.canvas);

		this._drawCanvas();
	}

	_image(){

		if ( !this.image ) return;

		let sx = 0,
			sy = 0,
			sWidth = this.image.width,
			sHeight = this.image.height,
			dx = this._options.paddings[3],
			dy = this._options.paddings[0],
			dWidth = this.canvas.width - this._options.paddings[1] - this._options.paddings[3],
			dHeight = this.canvas.height - this._options.paddings[0] - this._options.paddings[2],
			sRatio = sWidth / sHeight,
			dRatio = dWidth / dHeight,
			deltadx = 0,
			deltady = 0
		;

		if (sRatio < dRatio){
			deltadx = ( dWidth - (dHeight * sRatio)) / 2;
		} else {
			deltady = ( dHeight - (dWidth / sRatio)) / 2;
		}

		dx = dx + deltadx;
		dWidth = dWidth - (2 * deltadx);
		dy = dy + deltady;
		dHeight = dHeight - (2 * deltady);

		this.deltaPath.scale = dHeight / sHeight;
		this.deltaPath.x = this._options.paddings[3] + deltadx;
		this.deltaPath.y = this._options.paddings[0] + deltady;

		return {sx, sy, sWidth, sHeight, dx, dy, dWidth, dHeight}
	}

	_getPathPoint(point){
		return {
			x: (point[0] * this.deltaPath.scale) + this.deltaPath.x,
			y: (point[1] * this.deltaPath.scale) + this.deltaPath.y
		};
	}

	_drawCanvas(){

		let ctx = this.canvas.getContext('2d')
		;
		ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);

		if (this.image) {
			let {sx, sy, sWidth, sHeight, dx, dy, dWidth, dHeight} = this._image();
			ctx.drawImage(this.image, sx, sy, sWidth, sHeight, dx, dy, dWidth, dHeight);

			if (this.blend.path.length > 2) {

				ctx.beginPath();
				ctx.moveTo(this._getPathPoint(this.blend.path[0]).x, this._getPathPoint(this.blend.path[0]).y);
				for (let i = 1; i < this.blend.path.length; i++){
					let point = this._getPathPoint(this.blend.path[i]);
					ctx.lineTo(point.x, point.y);
				}
				ctx.closePath();

				ctx.fillStyle = this.blend.color;
				ctx.globalCompositeOperation = this.blend.mode;
				ctx.fill();

				if (this._options.debug) {
					ctx.lineWidth = 3;
					ctx.setLineDash([10, 10]);
					ctx.strokeStyle = "#FF0000";
					ctx.stroke();
				}



			}
		}

		window.requestAnimationFrame(this._drawCanvas.bind(this));
	}


	init(){
		window.requestAnimationFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame || window.webkitRequestAnimationFrame || window.msRequestAnimationFrame;

		this.el = document.querySelectorAll(this._options.element)[0];

		if ( this._options.image ) this.setImage(this._options.image);
		if ( this._options.path ) this.setPath(this._options.path);
		if ( this._options.mode ) this.setMode(this._options.mode);
		if ( this._options.color ) this.setColor(this._options.color);

		this._buildCanvas();

		if ( this._options.control ) {
			this._buildControl();
			document.querySelectorAll(this._options.control)[0].addEventListener('mousemove', (e)=>{
				let color = this.control.ctx.getImageData(e.offsetX, e.offsetY, 1, 1).data;
				if (color[3] !== 0) this.pick = `rgb(${color[0]},${color[1]},${color[2]})`;
			});
			document.querySelectorAll(this._options.control)[0].addEventListener('click', (e)=>{
				let color = this.control.ctx.getImageData(e.offsetX, e.offsetY, 1, 1).data;
				if (color[3] !== 0) this.blend.color = `rgb(${color[0]},${color[1]},${color[2]})`;
			});
		}
	}

	// public


	setImage(src){

		let img = new Image();

		img.onload = ()=>{
			this.image = img;
		};

		img.onerror = ()=>{
			console.error('image src not found');
		};

		img.src = src;
	}

	setColor(color){
		this.blend.color = color;
	}

	setPath(path){
		this.blend.path = path;
	}

	setMode(mode){
		this.blend.mode = mode;
	}
}
