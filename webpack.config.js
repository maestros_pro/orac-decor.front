
const path = require('path');
const fs = require('fs');
const glob  = require('glob');
const webpack = require('webpack');

const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");
const CopyWebpackPlugin = require('copy-webpack-plugin');
const ImageminPlugin = require('imagemin-webpack-plugin').default;
const imageminMozjpeg = require('imagemin-mozjpeg');
const autoprefixer = require('autoprefixer');

const ENV = process.env.NODE_ENV;
const LOCAL = ENV === 'local';
const DEV = ENV === 'development';
const PROD = ENV === 'production';

const PATH = {
	src: path.resolve(__dirname, 'source'),
	dist: path.resolve(__dirname, 'public'),
};

// const entry =

let pages = [];
glob.sync(PATH.src + '/pages/*.pug').reduce((x, y) => {
	let name = (/([-_\w]+).\w+$/gi).exec(y)[1];
	return pages.push(
		new HtmlWebpackPlugin({
			filename: `${name}.html`,
			template: `${PATH.src}/pages/${name}.pug`,
			hash: true,
			data: {
				kv: require('./source/data/kv.js'),
				examples: require('./source/data/examples.js'),
				videos: require('./source/data/videos.js'),
				gallery: require('./source/data/gallery.js')
			},
			inject: 'body'
		})
	);
}, {});


const config = {

	entry: glob.sync(PATH.src + '/js/*.js').reduce((x, y) => {
		// let p = (/([-_\w.]+).\w+$/gi).exec(y)[1];
		let p = y.match(/([-_\w.]+).\w+$/gi)[0].split('.').slice(0, -1).join('.');
		console.log(y, p);
		return Object.assign({
			//'@babel/polyfill': ['@babel/polyfill']
		}, {
			[p]: y,
		});
	}, {}),

	output: {
		path: PATH.dist,
		filename: 'js/[name].script.js'
	},

	stats: {
		colors: true,
		errorDetails: true,
	},

	mode: ENV === 'production' ? 'production' : 'development',

	devServer: {
		contentBase: PATH.dist,
		compress: true,
		hot: true,
		inline: true,
		open: true,
		historyApiFallback: true
	},

	plugins: [
		new webpack.DefinePlugin({
			'isLocal': JSON.stringify(LOCAL),
			'isDev': JSON.stringify(DEV),
			'isProd': JSON.stringify(PROD),
			'process.env': {
				NODE_ENV: JSON.stringify(ENV)
			}
		}),
		new MiniCssExtractPlugin({
			filename: 'css/[name].style.css',
			chunkFilename: '[id].css',
		}),
		new CopyWebpackPlugin([
			// { from: `${PATH.src}/fonts`, to: `${PATH.dist}/fonts`},
			{ from: `${PATH.src}/img`, to: `${PATH.dist}/img`},
			{ from: `${PATH.src}/data`, to: `${PATH.dist}/data`}
		]),
		new ImageminPlugin({
			test: /\.(jpe?g|png|gif|svg)$/i,
			disable: LOCAL, // Disable during development
			plugins: [
				imageminMozjpeg({
					quality: 65,
					progressive: true
				})
			],
			jpegtran: null,
			optipng: null,
			pngquant: {
				quality: '65-90',
				speed: 4
			},
			gifsicle: {
				interlaced: false,
			}
		})
		].concat(pages),

	devtool: 'source-map',

	module:  {
		rules: [
			{
				test: /\.js$/,
				exclude: /(node_modules|bower_components)/,
				use: [{
					loader: "babel-loader",
					options: {
						presets: [[
							"@babel/preset-env",
							{
								targets: {
									browsers: [
										'Chrome >= 42',
										'Safari >= 10.1',
										'iOS >= 10.3',
										'Firefox >= 50',
										'Edge >= 12',
										'ie >= 10',
									],
								}
							}
						]]
					}
				}]
			},
			{
				test: /\.pug$/,
				use: [{
					loader: "pug-loader",
					options: {
						pretty: true
					}
				}]
			},
			{
				test: /\.(gif|png|jpe?g|svg)$/i,
				// exclude: /(sprites|svg)/,
				include: [path.resolve(__dirname, "source/img")],
				use: [
					{
						loader: 'file-loader',
						options: {
							name: '[name].[ext]',
							// outputPath: 'img/',
							useRelativePath: true
						}
					},
					{
						loader: 'image-webpack-loader',
						options: {
							mozjpeg: {
								progressive: true,
								quality: 65
							},
							// optipng.enabled: false will disable optipng
							optipng: {
								enabled: false,
							},
							pngquant: {
								quality: '65-90',
								speed: 4
							},
							gifsicle: {
								interlaced: false,
							},
							// https://github.com/svg/svgo
							svgo: {
								removeComments: true,
								removeDoctype: true,
								removeTitle: true,
								removeMetadata: true,
								removeEmptyContainers: true,
								removeAttrs: true,
								removeScriptElement: true
							}
						}
					}
				]
			},
			{
				test: /\.scss$/,
				// include: [path.resolve(__dirname, "source/css")],
				use: [
					LOCAL ?
						{loader: 'style-loader'} :
						{
							loader: MiniCssExtractPlugin.loader,
							options: {
								publicPath: '../',
								sourceMap: true
							}
						},
					{
						loader: 'css-loader',
						options: {
							minimize: true
						}
					},
					{
						loader: 'postcss-loader',
						options: {
							plugins: [
								autoprefixer({
									browsers:['ie >= 8', 'last 4 version']
								})
							],
							name: LOCAL ? '[name][hash].[ext]' : '[name].[ext]',
							sourceMap: true,
							useRelativePath: true
						}
					},
					'resolve-url-loader',
					{
						loader: 'sass-loader',
						options: {
							sourceMap: true
						},
					}
				]
			},
			{
				test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
				include: [path.resolve(__dirname, "source/fonts")],
				use: [{
					loader: 'file-loader',
					options: {
						name: '[name].[ext]',
						useRelativePath: true
					}
				}]
			},
			{
				type: 'javascript/auto',
				test: /\.json$/,
				// include: [path.resolve(__dirname, "source/data")],
				use: [{
					loader: 'file-loader',
					options: {
						name: '[name].[ext]',
						useRelativePath: true
					}
				}]
			}
		]
	},

	optimization: PROD ? {
		minimizer: [
			new UglifyJsPlugin({
				sourceMap: true,
				uglifyOptions: {
					//ecma: 5,
					compress: {
						inline: true,
						warnings: false,
						drop_console: true,
						unsafe: true
					},
				},
			}),
			new OptimizeCSSAssetsPlugin({
				cssProcessorPluginOptions: {
					preset: ['default', { discardComments: { removeAll: true } }],
				},
			})
		],
	} : {}
};

module.exports = config;