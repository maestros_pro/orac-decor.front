module.exports = [
	{
		title: 'Создайте свои уникальные устройства.',
		image: 'img/gallery/01-main.jpg',
		points: [
			{
				number: 1,
				left: 6.91,
				top: 12.44
			},
			{
				number: 2,
				left: 12.46,
				top: 88.07
			},
			{
				number: 3,
				left: 61.78,
				top: 10.39
			},
			{
				number: 4,
				left: 72.88,
				top: 4.09
			}
		],
		consist: [
			{
				number: 1,
				title: 'C357',
				image: ['img/gallery/consist/c357.jpg']
			},
			{
				number: 2,
				title: 'DX157-2300',
				image: ['img/gallery/consist/dx157-2300.jpg']
			},
			{
				number: 3,
				title: 'C380',
				image: ['img/gallery/consist/c380.jpg']
			},
			{
				number: 4,
				title: 'C991',
				image: ['img/gallery/consist/c991.jpg']
			}
		]
	},
	{
		title: 'Создайте свои уникальные устройства.',
		image: 'img/gallery/02-main.jpg',
		points: [
			{
				number: 1,
				left: 36.23,
				top: 56.9
			},
			{
				number: 2,
				left: 36.23,
				top: 65.93
			},
			{
				number: 3,
				left: 36.23,
				top: 85.52
			},
			{
				number: 1,
				left: 63.25,
				top: 3.92
			},
			{
				number: 4,
				left: 60.31,
				top: 5.11
			},
			{
				number: 1,
				left: 92.46,
				top: 50.94
			},
			{
				number: 3,
				left: 92.46,
				top: 86.20
			}
		],
		consist: [
			{
				number: 1,
				title: 'P8030',
				image: ['img/gallery/consist/p8030.jpg']
			},
			{
				number: 2,
				title: 'P4020',
				image: ['img/gallery/consist/p4020.jpg']
			},
			{
				number: 3,
				title: 'SX118',
				image: ['img/gallery/consist/sx118.jpg']
			},
			{
				number: 4,
				title: 'C339',
				image: ['img/gallery/consist/c339.jpg']
			}
		]
	},
	{
		title: 'Вдохните жизнь в серые стены.',
		image: 'img/gallery/03-main.jpg',
		points: [
			{
				number: 1,
				left: 28.17,
				top: 85.86
			},
			{
				number: 2,
				left: 32.88,
				top: 77.55
			},
			{
				number: 3,
				left: 70.37,
				top: 7.5
			},
			{
				number: 2,
				left: 80.21,
				top: 11.24
			}
		],
		consist: [
			{
				number: 1,
				title: 'SX122',
				image: ['img/gallery/consist/sx122.jpg']
			},
			{
				number: 2,
				title: 'P8030',
				image: ['img/gallery/consist/p8030.jpg']
			},
			{
				number: 3,
				title: 'C339',
				image: ['img/gallery/consist/c339.jpg']
			}
		]
	},
	{
		title: 'Вдохните жизнь в серые стены.',
		image: 'img/gallery/04-main.jpg',
		points: [
			{
				number: 1,
				left: 76.96,
				top: 11.41
			},
			{
				number: 1,
				left: 72.04,
				top: 41.57
			},
			{
				number: 1,
				left: 97.07,
				top: 66.65
			}
		],
		consist: [
			{
				number: 1,
				title: 'd503',
				image: ['img/gallery/consist/d503.jpg']
			}
		]
	},
	{
		title: 'Кто сказал, что коридор должен быть самым тёмным и скучным местом в доме?',
		image: 'img/gallery/05-main.jpg',
		points: [
			{
				number: 1,
				left: 35.08,
				top: 26.41
			},
			{
				number: 2,
				left: 32.04,
				top: 89.10
			},
			{
				number: 3,
				left: 81.68,
				top: 6.3
			},
			{
				number: 4,
				left: 89.01,
				top: 42.59
			},
			{
				number: 5,
				left: 89.01,
				top: 75.81
			},
			{
				number: 6,
				left: 90.37,
				top: 83.48
			}
		],
		consist: [
			{
				number: 1,
				title: 'C381',
				image: ['img/gallery/consist/c381.jpg']
			},
			{
				number: 2,
				title: 'SX181',
				image: ['img/gallery/consist/sx181.jpg']
			},
			{
				number: 3,
				title: 'С341',
				image: ['img/gallery/consist/c341.jpg']
			},
			{
				number: 4,
				title: 'DX121-2300',
				image: ['img/gallery/consist/dx121-2300.jpg']
			},
			{
				number: 5,
				title: 'P8030',
				image: ['img/gallery/consist/p8030.jpg']
			},
			{
				number: 6,
				title: 'SX118',
				image: ['img/gallery/consist/sx118.jpg']
			}
		]
	},
	{
		title: 'Выигрышная комбинация.',
		image: 'img/gallery/06-main.jpg',
		points: [
			{
				number: 1,
				left: 41.36,
				top: 23
			},
			{
				number: 1,
				left: 41.36,
				top: 28.11
			},
			{
				number: 2,
				left: 44.71,
				top: 84.67
			},
			{
				number: 3,
				left: 60.00,
				top: 13.46
			},
			{
				number: 4,
				left: 58.53,
				top: 37.48
			},
			{
				number: 5,
				left: 72.36,
				top: 61.50
			},
			{
				number: 6,
				left: 67.12,
				top: 78.19
			},
		],
		consist: [
			{
				number: 1,
				title: 'C380',
				image: ['img/gallery/consist/c380.jpg']
			},
			{
				number: 2,
				title: 'SX181',
				image: ['img/gallery/consist/sx181.jpg']
			},
			{
				number: 3,
				title: 'С902',
				image: ['img/gallery/consist/c902.jpg']
			},
			{
				number: 4,
				title: 'P8020',
				image: ['img/gallery/consist/p8020.jpg']
			},
			{
				number: 5,
				title: 'P8050',
				image: ['img/gallery/consist/p8050.jpg']
			},
			{
				number: 6,
				title: 'SX118',
				image: ['img/gallery/consist/sx118.jpg']
			}
		]
	},
	{
		title: 'Установите скрытое освещение внутрь карниза для создания эффекта дополнительного пространства.',
		image: 'img/gallery/07-main.jpg',
		points: [
			{
				number: 1,
				left: 32.88,
				top: 9.37
			},
			{
				number: 2,
				left: 43.87,
				top: 16.87
			},
			{
				number: 3,
				left: 47.76,
				top: 85.86
			},
			{
				number: 4,
				left: 72.15,
				top: 21.64
			},
			{
				number: 5,
				left: 72.15,
				top: 34.24
			},
			{
				number: 3,
				left: 68.17,
				top: 84.50
			},
		],
		consist: [
			{
				number: 1,
				title: 'С393',
				image: ['img/gallery/consist/c393.jpg']
			},
			{
				number: 2,
				title: 'С383',
				image: ['img/gallery/consist/c383.jpg']
			},
			{
				number: 3,
				title: 'SX181',
				image: ['img/gallery/consist/sx181.jpg']
			},
			{
				number: 4,
				title: 'C990+2XP9900',
				image: [
					'img/gallery/consist/c990.jpg',
					'img/gallery/consist/2xp9900.jpg'
				]
			},
			{
				number: 5,
				title: 'С381',
				image: ['img/gallery/consist/c381.jpg']
			}
		]
	},
	{
		title: 'Превратите стену в чистый холст.',
		image: 'img/gallery/08-main.jpg',
		points: [
			{
				number: 1,
				left: 16.34,
				top: 5.79
			},
			{
				number: 2,
				left: 16.34,
				top: 79.39
			},
			{
				number: 3,
				left: 16.34,
				top: 87.83
			},
			{
				number: 4,
				left: 16.34,
				top: 95.74
			},
		],
		consist: [
			{
				number: 1,
				title: 'С219',
				image: ['img/gallery/consist/c219.jpg']
			},
			{
				number: 2,
				title: 'P8020',
				image: ['img/gallery/consist/p8020.jpg']
			},
			{
				number: 3,
				title: 'PX103',
				image: ['img/gallery/consist/px103.jpg']
			},
			{
				number: 4,
				title: 'SX155',
				image: ['img/gallery/consist/sx155.jpg']
			}
		]
	},
	{
		title: 'Наши молдинги превратят стены в произведение искусств.',
		image: 'img/gallery/09-main.jpg',
		points: [
			{
				number: 1,
				left: 26.81,
				top: 47.53
			},
			{
				number: 2,
				left: 41.47,
				top: 52.81
			},
			{
				number: 3,
				left: 41.47,
				top: 59.45
			},
			{
				number: 4,
				left: 41.47,
				top: 75.30
			},
			{
				number: 5,
				left: 85.45,
				top: 23.85
			},
			{
				number: 3,
				left: 92.88,
				top: 64.57
			},
		],
		consist: [
			{
				number: 1,
				title: 'P9010',
				image: ['img/gallery/consist/p9010.jpg']
			},
			{
				number: 2,
				title: 'P4020',
				image: ['img/gallery/consist/p4020.jpg']
			},
			{
				number: 3,
				title: 'P8020',
				image: ['img/gallery/consist/p8020.jpg']
			},
			{
				number: 4,
				title: 'SX118',
				image: ['img/gallery/consist/sx118.jpg']
			},
			{
				number: 5,
				title: 'C219',
				image: ['img/gallery/consist/c219.jpg']
			}
		]
	},
	{
		title: 'Мгновенный эффект.',
		image: 'img/gallery/10-main.jpg',
		points: [
			{
				number: 1,
				left: 81.99,
				top: 4.43
			},
			{
				number: 2,
				left: 81.99,
				top: 12.61
			},
			{
				number: 3,
				left: 81.99,
				top: 33.39
			},
			{
				number: 4,
				left: 81.99,
				top: 78.71
			},
			{
				number: 5,
				left: 81.99,
				top: 90.97
			},
		],
		consist: [
			{
				number: 1,
				title: 'P8030',
				image: ['img/gallery/consist/p8030.jpg']
			},
			{
				number: 2,
				title: 'C344',
				image: ['img/gallery/consist/c344.jpg']
			},
			{
				number: 3,
				title: 'P8020',
				image: ['img/gallery/consist/p8020.jpg']
			},
			{
				number: 4,
				title: 'P4020',
				image: ['img/gallery/consist/p4020.jpg']
			},
			{
				number: 5,
				title: 'SX118',
				image: ['img/gallery/consist/sx118.jpg']
			}
		]
	}
];