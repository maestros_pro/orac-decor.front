import * as THREE from "three";
let OrbitControls = require('three-orbit-controls')(THREE);
import {OBJLoader} from 'three-obj-mtl-loader';

export default class Model3D {
	constructor(options) {

		this._polyfill();

		this.canvas = null;

		Object.assign(this._options = {}, this._default(), options);
		if (document.readyState === 'loading') {
			document.addEventListener('DOMContentLoaded', () => {
				this.init();
			});
		} else {
			this.init();
		}
	}

	_polyfill() {
		if (!Object.assign) {
			Object.defineProperty(Object, 'assign', {
				enumerable: false,
				configurable: true,
				writable: true,
				value: function (target, firstSource) {
					'use strict';
					if (target === undefined || target === null) {
						throw new TypeError('Cannot convert first argument to object');
					}

					let to = Object(target);
					for (let i = 1; i < arguments.length; i++) {
						let nextSource = arguments[i];
						if (nextSource === undefined || nextSource === null) {
							continue;
						}

						let keysArray = Object.keys(Object(nextSource));
						for (let nextIndex = 0, len = keysArray.length; nextIndex < len; nextIndex++) {
							let nextKey = keysArray[nextIndex],
								desc = Object.getOwnPropertyDescriptor(nextSource, nextKey);
							if (desc !== undefined && desc.enumerable) {
								to[nextKey] = nextSource[nextKey];
							}
						}
					}
					return to;
				}
			});
		}

		window.requestAnimationFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame || window.webkitRequestAnimationFrame || window.msRequestAnimationFrame;

	}

	_default() {
		return {
			element: false
		}
	}

	_draw(){


		this.renderer = new THREE.WebGLRenderer( { alpha: true, antialias: true } );
		this.renderer.setSize( this.el.clientWidth, this.el.clientHeight );
		this.el.appendChild( this.renderer.domElement );

		this.scene = new THREE.Scene();
		this.scene.add( new THREE.AmbientLight( 0xffffff, 0.2 ) );

		this.camera = new THREE.PerspectiveCamera( 20, this.el.clientWidth / this.el.clientHeight, 1, 500 );
		this.camera.up.set( 0, 0, 1 );
		this.camera.position.set( 0, 50, 0 );
		this.scene.add( this.camera );

		this.controls = new OrbitControls( this.camera, this.renderer.domElement );
		this.controls.addEventListener( 'change', this._render.bind(this) );
		this.controls.minDistance = 300;
		this.controls.maxDistance = 300;
		this.controls.enableZoom = false;
		this.controls.enablePan = false;
		this.controls.target.set( 0, 0, 10 );
		this.controls.update();

		this.camera.add( new THREE.PointLight( 0xffffff, 0.8 ) );

		let loader = new OBJLoader();

		loader.load(this._options.object, ( object )=>{
			this.object = object;

			this.object.scale.set(this._options.objectScale, this._options.objectScale, this._options.objectScale);
			this.object.position.x = 0;
			this.object.position.y = 0;
			this.object.position.z = 30;
			this.object.rotation.x = -90 * Math.PI / 180;
			this.object.rotation.z = 180 * Math.PI / 180;
			this.object.rotation.y = 45 * Math.PI / 180;

			this.scene.add( this.object );
			this._render.bind(this);

			this.changeColor(this._options.objectColor);
		} );

	}

	_render(){
		this.renderer.render( this.scene, this.camera )
	}

	_animate(){
		window.requestAnimationFrame( this._animate.bind(this) );

		// this.mesh.rotation.x += 0.01;
		// this.mesh.rotation.y += 0.02;

		this._render();
	}

	changeColor(rgb){
		let material = new THREE.MeshLambertMaterial( {
			color: 'rgb('+ rgb +')',
			specular: 0x050505,
			roughness: 1,
			shininess: 100
		} );

		this.object.traverse( (child)=>{
			if ( child instanceof THREE.Mesh ) {
				child.material = material;
			}
		})
	}

	resize(){
		this.camera.aspect = this.el.clientWidth / this.el.clientHeight;
		this.camera.updateProjectionMatrix();

		this.renderer.setSize( this.el.clientWidth, this.el.clientHeight );
	}

	init(){

		if ( document.querySelectorAll(this._options.element).length ){
			this.el = document.querySelectorAll(this._options.element)[0];

			this._draw();
			this._animate();

			window.addEventListener( 'resize', this.resize.bind(this), false );
		}

	}


}
