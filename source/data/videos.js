module.exports = [
	{
		title: "Новая коллекция 2017 / 2018",
		text: "<p>Лепной декор способен сделать интерьер подчеркнуто стильным, вычурным, элегантным. В отличие от увесистого, тяжелого в работе гипса, полиуретан радует малым весом и простотой монтажа.</p><p>Полиуретановая лепнина на потолок от Orac Decor впечатлит разнообразием стилей. Сдержанный минимализм, легендарная классика, роскошное барокко, утонченная эклектика георгианской эпохи — вы обязательно найдете идеальный для себя вариант.</p>",
		preview: 'https://via.placeholder.com/800x900',
		videoType: 'html',
		videoSrc: 'data/video1080_5.mp4',
		elements: [
			{
				image: "img/product/ceiling/c340.png",
				title: "C340",
				text: "200 x 13,5 x 25,6 cm Purotouch®"
			},
			{
				image: "img/product/ceiling/c340.png",
				title: "C340",
				text: "200 x 13,5 x 25,6 cm Purotouch®"
			},
			{
				image: "img/product/ceiling/c340.png",
				title: "C340",
				text: "200 x 13,5 x 25,6 cm Purotouch®"
			},
			{
				image: "img/product/ceiling/c340.png",
				title: "C340",
				text: "200 x 13,5 x 25,6 cm Purotouch®"
			}
		]
	},
	{
		title: "La Rambla, Испания",
		text: "<p>Это один из самых уютных и популярных ресторанов на улице Рамбла в городе Сабадель.</p><p>Его проект был выполнен известным дизайнером интерьера из Барселоны. Дизайнер постарался максимально сохранить атмосферу старинного дома, разработав интерьер в стиле «винтаж» и умело сочетая многочисленные возможности продукции Orac Decor®. Комплект окон и дверей «под старину» органично вписался в интерьер ресторана.</p><p>Для создания приятной, расслабляющей атмосферы служит непрямое освещение.</p>",
		preview: 'https://img.youtube.com/vi/h463PIUTScg/maxresdefault.jpg',
		videoType: 'youtube',
		videoSrc: 'h463PIUTScg',
		elements: [
			{
				image: "img/product/ceiling/c340.png",
				title: "C340",
				text: "200 x 13,5 x 25,6 cm Purotouch®"
			},
			{
				image: "img/product/ceiling/c340.png",
				title: "C340",
				text: "200 x 13,5 x 25,6 cm Purotouch®"
			},
			{
				image: "img/product/ceiling/c340.png",
				title: "C340",
				text: "200 x 13,5 x 25,6 cm Purotouch®"
			},
			{
				image: "img/product/ceiling/c340.png",
				title: "C340",
				text: "200 x 13,5 x 25,6 cm Purotouch®"
			}
		]
	},
	{
		title: "La Rambla, Испания",
		text: "<p>Это один из самых уютных и популярных ресторанов на улице Рамбла в городе Сабадель.</p><p>Его проект был выполнен известным дизайнером интерьера из Барселоны. Дизайнер постарался максимально сохранить атмосферу старинного дома, разработав интерьер в стиле «винтаж» и умело сочетая многочисленные возможности продукции Orac Decor®. Комплект окон и дверей «под старину» органично вписался в интерьер ресторана.</p><p>Для создания приятной, расслабляющей атмосферы служит непрямое освещение.</p>",
		preview: 'https://i.vimeocdn.com/video/728637998_640.webp',
		videoType: 'vimeo',
		videoSrc: '292285205',
		elements: [
			{
				image: "img/product/ceiling/c340.png",
				title: "C340",
				text: "200 x 13,5 x 25,6 cm Purotouch®"
			},
			{
				image: "img/product/ceiling/c340.png",
				title: "C340",
				text: "200 x 13,5 x 25,6 cm Purotouch®"
			},
			{
				image: "img/product/ceiling/c340.png",
				title: "C340",
				text: "200 x 13,5 x 25,6 cm Purotouch®"
			},
			{
				image: "img/product/ceiling/c340.png",
				title: "C340",
				text: "200 x 13,5 x 25,6 cm Purotouch®"
			}
		]
	},
	{
		title: "La Rambla, Испания",
		text: "<p>Это один из самых уютных и популярных ресторанов на улице Рамбла в городе Сабадель.</p><p>Его проект был выполнен известным дизайнером интерьера из Барселоны. Дизайнер постарался максимально сохранить атмосферу старинного дома, разработав интерьер в стиле «винтаж» и умело сочетая многочисленные возможности продукции Orac Decor®. Комплект окон и дверей «под старину» органично вписался в интерьер ресторана.</p><p>Для создания приятной, расслабляющей атмосферы служит непрямое освещение.</p>",
		preview: 'https://img.youtube.com/vi/cbNbMwzEQBA/hqdefault.jpg',
		videoType: 'youtube',
		videoSrc: 'cbNbMwzEQBA',
		elements: [
			{
				image: "img/product/ceiling/c340.png",
				title: "C340",
				text: "200 x 13,5 x 25,6 cm Purotouch®"
			},
			{
				image: "img/product/ceiling/c340.png",
				title: "C340",
				text: "200 x 13,5 x 25,6 cm Purotouch®"
			},
			{
				image: "img/product/ceiling/c340.png",
				title: "C340",
				text: "200 x 13,5 x 25,6 cm Purotouch®"
			},
			{
				image: "img/product/ceiling/c340.png",
				title: "C340",
				text: "200 x 13,5 x 25,6 cm Purotouch®"
			}
		]
	},
	{
		title: "La Rambla, Испания",
		text: "<p>Это один из самых уютных и популярных ресторанов на улице Рамбла в городе Сабадель.</p><p>Его проект был выполнен известным дизайнером интерьера из Барселоны. Дизайнер постарался максимально сохранить атмосферу старинного дома, разработав интерьер в стиле «винтаж» и умело сочетая многочисленные возможности продукции Orac Decor®. Комплект окон и дверей «под старину» органично вписался в интерьер ресторана.</p><p>Для создания приятной, расслабляющей атмосферы служит непрямое освещение.</p>",
		preview: 'https://via.placeholder.com/850x1424',
		videoType: 'html',
		videoSrc: 'data/video1080_5.mp4',
		elements: [
			{
				image: "img/product/ceiling/c340.png",
				title: "C340",
				text: "200 x 13,5 x 25,6 cm Purotouch®"
			},
			{
				image: "img/product/ceiling/c340.png",
				title: "C340",
				text: "200 x 13,5 x 25,6 cm Purotouch®"
			},
			{
				image: "img/product/ceiling/c340.png",
				title: "C340",
				text: "200 x 13,5 x 25,6 cm Purotouch®"
			},
			{
				image: "img/product/ceiling/c340.png",
				title: "C340",
				text: "200 x 13,5 x 25,6 cm Purotouch®"
			}
		]
	}
];