import '../css/main.scss'


import $ from 'jquery'
import Popup from './modules/module.popup'
import svgmarker from './modules/module.svgmarker'
import Form from './modules/module.validate'
import Scroll from './modules/module.scrollnav'
import Inputmask from 'inputmask'
import Plyr from 'plyr'
import 'slick-carousel'
import 'malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min'
import ColorPicker from "./modules/module.colorpicker";
import Model3D from "./modules/module.3dmodel";


window.app = window.app || {};




$(function () {

	let model3d = new Model3D({
		element: '.advantage__graph-item',
		object: $('.advantage__graph-item').attr('data-model'),
		objectScale: 0.18,
		objectColor: '255, 255, 255',
	});

	let colorPicker = new ColorPicker({
		element: '.advantage__graph-control',
		gradient: [
			'#ffffff',
			'#f30610',
			'#e900ff',
			'#5569ff',
			'#59ebfe',
			'#2dff00',
			'#f7ff03',
			'#e30a0c'
		]
	});

	let $b = $('body'),
		player = new Plyr('#player')
	;

	(()=>{
		let head = document.head || document.getElementsByTagName('head')[0],
			padding = window.innerWidth - document.documentElement.clientWidth,
			style = document.createElement('style');
		style.innerHTML = `.oh .wrapper{padding-right: ${padding}px;}`;
		head.appendChild(style);
	})();

	window.app.popup = new Popup({
		onPopupClose: (p)=>{
			$b.removeClass('is-popup-show');
			$(p).removeClass('is-show');
			try { player.stop() } catch (e){}
		},
		onPopupOpen: (p)=>{
			$b.addClass('is-popup-show');
			setTimeout(()=>{$(p).addClass('is-show');}, 10);
		}
	});

	$b

		.on('click', '.nav__handler, .nav__overlay', function(){
			$b.toggleClass('is-nav-active');
		})

		.on('click', '.nav__menu-item', function(e){
			e.preventDefault();
			$('html,body').animate({scrollTop:$($(this).attr('href')).offset().top + 1}, 500);
			$b.toggleClass('is-nav-active');
		})

		.on('click', '.kv__menu-item', function(){
			let $t = $(this),
				group = $t.attr('data-group'),
				$bg = $('.kv__bg-item').filter('[data-group = ' + group + ']'),
				$slider = $('.kv__carousel-item').filter('[data-group = ' + group + ']')
			;

			if ( $t.hasClass('is-active') ) return false;

			$t.addClass('is-active').siblings().removeClass('is-active');
			$bg.addClass('is-active').siblings().removeClass('is-active');
			$slider.addClass('is-active').siblings().removeClass('is-active');

		})

		.on('click', '.advantage__item-title', function(){
			let $t = $(this),
				$item = $t.closest('.advantage__item'),
				$active = $item.siblings('.is-active')
			;

			if ( $item.hasClass('is-active') ) return false;

			$active
				.removeClass('is-active')
				.addClass('out-active')
				.find('.advantage__item-text')
				.slideUp(300, function(){
					$(this).removeAttr('style')
						.closest('.advantage__item')
						.removeClass('out-active')
						.removeClass('in-active')
					;
			});

			$item.addClass('in-active')
				.find('.advantage__item-text')
				.slideDown(300, function(){
					$(this).removeAttr('style')
						.closest('.advantage__item')
						.addClass('is-active')
						.removeClass('in-active')
					;
			});


		})

		.on('mouseover', '[data-point]', function(){
			let $t = $(this),
				$gallery = $t.closest('.gallery__item'),
				point = $t.attr('data-point'),
				$item = $gallery.find('[data-point=' + point + ']'),
				$main = $item.closest('.gallery__main'),
				$aside = $item.closest('.gallery__aside')
			;

			$item.addClass('is-active');

			if ( $t.closest('.gallery__aside').length ){
				$main.find($item).addClass("is-animate");
			} else {
				$aside.find('.gallery__aside-list').mCustomScrollbar("scrollTo", $aside.find($item));
				$aside.find($item).addClass("is-animate");
			}

		})

		.on('mouseleave', '[data-point]', function(){
			let $t = $(this),
				$gallery = $t.closest('.gallery')
			;
			$gallery.find('[data-point]').removeClass('is-active is-animate');
		})

		.on('click', '.gallery__zoom, .gallery__overlay', function(){
			let $t = $(this),
				$gallery = $t.closest('.gallery')
			;
			$gallery.toggleClass('gallery_open');
			$b.toggleClass('oh');
			$('html,body').animate({scrollTop:$gallery.offset().top - 30}, 500);
		})

		.on('click', '[data-product-id]', function(){
			let $t = $(this),
				id = $t.attr('data-product-id')
			;

			$('.popup_product').find('.popup__content').html();

			if ( $t.hasClass('is-loading') ) return false;
			$t.addClass('is-loading');

			$.ajax({
				url: window.productAction,
				cache: false,
				data: {id: id}
			}).done(function(data){
				console.info(data);
				window.app.popup.open('.popup_product', data)
			}).always(function(){
				$t.removeClass('is-loading');
			}).fail(function(err){

			});

		})

		.on('click', '[data-video-src]', function(){
			let $t = $(this),
				type = $t.attr('data-video-type'),
				src = $t.attr('data-video-src')
				// html = `<div class="popup__video"><video src="${src}" id="player"></video></div>`
			;

			window.app.popup.open('.popup_video');

			setPlayerSrc(src, type);

			$b.css({overflow: 'hidden'});

		})

		.on('click', '.popup__video-control-arrow', function(){
			let $t = $(this),
				isPrev = $t.hasClass('popup__video-control-arrow_prev'),
				isNext = $t.hasClass('popup__video-control-arrow_next')

			;

			if ( $t.hasClass('is-disable') ) return false;


			if (isPrev){
				$('.videos__info-list').find('.slick-arrow_prev').click();
			} else if ( isNext ){
				$('.videos__info-list').find('.slick-arrow_next').click();
			}

			let $slide = $('.videos__preview-list').find('.slick-active').find('[data-video-src]'),
				src = $slide.attr('data-video-src'),
				type = $slide.attr('data-video-type')
			;

			setPlayerSrc(src, type);
			$b.css({overflow: 'hidden'});

		})
	;

	function setPlayerSrc(src, type){
		player.stop();

		let sources = [];

		if ( type === 'html'){
			sources = [
				{
					src: src,
					type: 'video/mp4'
				},
			]
		} else {
			sources = [
				{
					src: src,
					provider: type,
				},
			]
		}

		player.source = {
			type: 'video',
			title: '',
			sources
		};
	}

	$.fn.cssNumber = function(prop){
		var v = parseInt(this.css(prop),10);
		return isNaN(v) ? 0 : v;
	};

	let colorPickerHandler = {
		top: 0,
		offset: 0,
		clicked: false
	};
	$b
		.on('mousedown', '.advantage__graph-control-handler', (e)=>{
			colorPickerHandler.top = e.pageY;
			colorPickerHandler.clicked = true;
		})

		.on('mouseup', (e)=>{
			colorPickerHandler.clicked = false;
		})

		.on('mouseover', '.advantage__graph-control-handler', (e)=>{
			// colorPickerHandler.clicked = false;
		})

		.on('mousemove', (e)=>{
			let $t = $('.advantage__graph-control-handler'),
				$line = $t.closest('.advantage__graph-control')
			;

			if ( colorPickerHandler.clicked ){

				colorPickerHandler.offset = e.pageY - colorPickerHandler.top;

				let pos = $t.cssNumber('top'),
					max = $line.cssNumber('height'),
					offset	= colorPickerHandler.offset,
					top = pos + offset;


				if ( top < 1 ){
					top = 1;
				} else if ( top > max - 1 ){
					top = max - 1;
				}

				let color = colorPicker.getColor(1, top);

				$t.css({
					top: top + 'px',
					color: '#' + color.hex
				});

				// console.info(pos, max, top);
				colorPickerHandler.top = e.pageY;

				model3d.changeColor(color.rgb);
			}

		})

		.on('click', '.advantage__graph-control', (e)=>{

			if ( !colorPickerHandler.clicked && !$(e.target).closest('.advantage__graph-control-handler').length ){
				let color = colorPicker.getColor(1, e.offsetY);
				$('.advantage__graph-control-handler').css({
					color: '#' + color.hex
				}).animate({
					top: e.offsetY,
				});
				model3d.changeColor(color.rgb);
			}
		})

	;


	let activeMenu = null;
	new Scroll({
		sections: [
			{
				element: '.kv__top',
				name: 'kv'
			},
			{
				element: '.kv__bottom',
				name: 'kv'
			},
			{
				element: '.welcome',
				name: 'welcome'
			},
			{
				element: '.advantage',
				name: 'advantage'
			},
			{
				element: '.example__top',
				name: 'example'
			},
			{
				element: '.example__composition',
				name: 'example'
			},
			{
				element: '.videos',
				name: 'videos'
			},
			{
				element: '.contacts',
				name: 'contacts'
			},
		],
		offset: 60,
		onChange: (e, i)=>{


			if ( i === 1 || i === 3 || i === 5 || i === 6 ){
				$('.nav').addClass('nav_dark');
			} else {
				$('.nav').removeClass('nav_dark');
			}

			if ( activeMenu !== e){
				activeMenu = e;

				$('[data-section]').removeClass('is-active');
				$('[data-section=' + activeMenu + ']').addClass('is-active');
			}
		},
		onOut: (e)=>{
			console.info('onOut', e);
		}
	});


	// example

	$('.example__info-list')
		.on('init', function (event, slick) {
			let lngth = (slick.$slides.length.toString().length < 2 ? '0' : '') + slick.$slides.length.toString(),
				count = '01'
			;
			$('.example__info-counter').html(`${count} | ${lngth}`);
		})
		.on('beforeChange', function (event, slick, currentSlide, nextSlide) {
			let lngth = (slick.$slides.length.toString().length < 2 ? '0' : '') + slick.$slides.length.toString(),
				count = ((nextSlide + 1).toString().length < 2 ? '0' : '') + (nextSlide + 1).toString()
			;
			$('.example__info-counter').html(`${count} | ${lngth}`);
			$('.example__composition-item').removeClass('is-active').eq(nextSlide).addClass('is-active');
		})
		.on('afterChange', function () {

		})
		.slick({
			infinite: false,
			speed: 300,
			swipe: true,
			swipeToSlide: true,
			arrows: true,
			slidesToShow: 1,
			slidesToScroll: 1,
			fade: true,
			adaptiveHeight: true,
			prevArrow: '<svg class="slick-arrow_prev" xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20"><path d="M13.678,20l-9.7-10,9.7-10,1.358,1.3L6.6,10l8.44,8.7Z" transform="translate(0.5)"/></svg>',
			nextArrow: '<svg class="slick-arrow_next" xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20"><path d="M5.346,0l9.647,10L5.346,20,4,18.7,12.388,10,4,1.3Z" transform="translate(0.5)"/></svg>',
			asNavFor: '.example-slider'
		});


	$('.example__preview-list')
		.slick({
			speed: 300,
			infinite: false,
			swipe: false,
			swipeToSlide: false,
			arrows: false,
			slidesToShow: 1,
			slidesToScroll: 1,
			fade: true,
			adaptiveHeight: false,
			asNavFor: '.example-slider'
		});


	$('.gallery__list')
		.slick({
			infinite: false,
			speed: 300,
			swipe: false,
			swipeToSlide: false,
			arrows: true,
			slidesToShow: 1,
			slidesToScroll: 1,
			prevArrow: '<div class="slick-arrow_prev"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20"><path d="M13.678,20l-9.7-10,9.7-10,1.358,1.3L6.6,10l8.44,8.7Z" transform="translate(0.5)"/></svg></div>',
			nextArrow: '<div class="slick-arrow_next"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20"><path d="M5.346,0l9.647,10L5.346,20,4,18.7,12.388,10,4,1.3Z" transform="translate(0.5)"/></svg></div>',
			fade: true,
			adaptiveHeight: false,
			asNavFor: '.gallery-slider'
		});

	$('.gallery__preview-list')
		.slick({
			infinite: false,
			speed: 300,
			swipe: false,
			swipeToSlide: false,
			arrows: false,
			slidesToShow: 1,
			focusOnSelect: true,
			slidesToScroll: 1,
			asNavFor: '.gallery-slider'
		});

	// videos

	$('.videos__info-list')
		.on('init', function (event, slick) {
			let lngth = (slick.$slides.length.toString().length < 2 ? '0' : '') + slick.$slides.length.toString(),
				count = '01'
			;
			$('.videos__info-counter').html(`${count} | ${lngth}`);

			$('.popup__video-control-arrow_prev').addClass('is-disable');
		})
		.on('beforeChange', function (event, slick, currentSlide, nextSlide) {
			let lngth = (slick.$slides.length.toString().length < 2 ? '0' : '') + slick.$slides.length.toString(),
				count = ((nextSlide + 1).toString().length < 2 ? '0' : '') + (nextSlide + 1).toString()
			;
			$('.videos__info-counter').html(`${count} | ${lngth}`);

			console.info(slick.$slides.length, nextSlide);

			$('.popup__video-control-arrow').removeClass('is-disable');
			if ( nextSlide === 0 ){
				$('.popup__video-control-arrow_prev').addClass('is-disable');
			} else if ( slick.$slides.length - 1 === nextSlide ){
				$('.popup__video-control-arrow_next').addClass('is-disable');
			}
		})
		.on('afterChange', function () {

		})
		.slick({
			infinite: false,
			speed: 300,
			swipe: true,
			swipeToSlide: true,
			arrows: true,
			slidesToShow: 1,
			slidesToScroll: 1,
			fade: true,
			adaptiveHeight: true,
			prevArrow: '<svg class="slick-arrow_prev" xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20"><path d="M13.678,20l-9.7-10,9.7-10,1.358,1.3L6.6,10l8.44,8.7Z" transform="translate(0.5)"/></svg>',
			nextArrow: '<svg class="slick-arrow_next" xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20"><path d="M5.346,0l9.647,10L5.346,20,4,18.7,12.388,10,4,1.3Z" transform="translate(0.5)"/></svg>',
			asNavFor: '.videos-slider'
		});


	$('.videos__preview-list')
		.slick({
			speed: 300,
			swipe: false,
			infinite: false,
			swipeToSlide: false,
			arrows: false,
			slidesToShow: 1,
			slidesToScroll: 1,
			fade: true,
			adaptiveHeight: false,
			asNavFor: '.videos-slider'
		});







	// products__carousel
	$('.products__carousel').each(function () {

		let $slider = $(this);

		$slider
			.on('init', function (event, slick) {

			})
			.on('beforeChange', function (event, slick, currentSlide, nextSlide) {

			})
			.on('afterChange', function () {

			})
			.slick({
				infinite: false,
				speed: 300,
				swipe: true,
				swipeToSlide: true,
				arrows: true,
				slidesToShow: 3,
				slidesToScroll: 1,
				// adaptiveHeight: false,
				prevArrow: '<svg class="slick-arrow_prev" xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20"><path d="M13.678,20l-9.7-10,9.7-10,1.358,1.3L6.6,10l8.44,8.7Z" transform="translate(0.5)"/></svg>',
				nextArrow: '<svg class="slick-arrow_next" xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20"><path d="M5.346,0l9.647,10L5.346,20,4,18.7,12.388,10,4,1.3Z" transform="translate(0.5)"/></svg>',
				mobileFirst: true,
				responsive: [
					{
						breakpoint: 1599,
						settings: {
							slidesToShow: 4
						}
					}
				]
			});

	});




	$('.popup__gallery').each(function () {
		let $t = $(this),
			$slider = $t.find('.popup__gallery-list'),
			$preview = $t.find('.popup__gallery-preview'),
			$thumbnail = $t.find('.popup__gallery-thumbnail')
		;

		$t.find('.popup__gallery-info').mCustomScrollbar();
		$thumbnail.eq(0).addClass('is-active');

		let $prev = $('<div class="slick-arrow_prev"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20"><path d="M13.678,20l-9.7-10,9.7-10,1.358,1.3L6.6,10l8.44,8.7Z" transform="translate(0.5)"/></svg></div>'),
			$next = $('<div class="slick-arrow_next"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20"><path d="M5.346,0l9.647,10L5.346,20,4,18.7,12.388,10,4,1.3Z" transform="translate(0.5)"/></svg></div>')
		;

		$slider
			.on('init', function (event, slick) {

				slick.$slides.each(function(){
					console.info($(this).find('.popup__gallery-image'), $prev, $next);
					$(this).find('.popup__gallery-image').append($prev.clone( true ));
					$(this).find('.popup__gallery-image').append($next.clone( true ));
				})

			})
			.on('beforeChange', function (event, slick, currentSlide, nextSlide) {
				$thumbnail.removeClass('is-active').eq(nextSlide).addClass('is-active');
			})
			.on('afterChange', function () {

			})
			.slick({
				infinite: true,
				speed: 300,
				swipe: false,
				swipeToSlide: false,
				arrows: true,
				slidesToShow: 1,
				slidesToScroll: 1,
				prevArrow: $prev,
				nextArrow: $next,
				fade: true,
				adaptiveHeight: false
			});

		$thumbnail.on('click', function () {
			$slider.slick('slickGoTo', $(this).index());
		})
	});

	$b.on('click', '[data-open-gallery]', function () {
		let $t = $(this),
			index = $t.attr('data-open-gallery')
		;

		let p = window.app.popup.open('.popup_gallery');
		$(p).find('.popup__gallery-list').slick('slickGoTo',index);

		$(window).trigger('resize');

	});

	// gallery




	$('.gallery__aside-list').mCustomScrollbar();
	$('.m_custom_ccrollbar').mCustomScrollbar();





	$.getJSON('../../data/map.json', function (styles) {

		if ( window.google ){

			let SVGMarker = svgmarker.init();

			let map = new google.maps.Map($('.contacts__map')[0], {
				center: new google.maps.LatLng(59.9105067,30.4459623),
				zoom: 15,
				disableDefaultUI: true,
				zoomControl: false,
				scrollwheel: false,
				styles: styles,
				backgroundColor: 'none'
			});

			// See Github for class https://github.com/defvayne23/SVGMarker
			let marker = new SVGMarker({
				map: map,
				position: new google.maps.LatLng(59.9105067,30.4459623),
				icon: {
					anchor: new google.maps.Point(50, 50),
					size: new google.maps.Size(50,50),
					url: '/img/svg/marker.svg'
				}
			})
		}
	});



	//////////////////////////////////////
	//
	//////////////////////////////////////




	const form = new Form();


	$('input[data-mask]').each(function () {
		let $t = $(this),
			inputmask = new Inputmask({
			mask: $t.attr('data-mask'),		//'+7 (999) 999-99-99',
			showMaskOnHover: false,
			onincomplete: function() {
				this.value = '';
				$t.closest('.form__field').removeClass('f-filled');
			}
		});
		inputmask.mask($t[0]);
	});

	$b
		.on('click', '[data-scroll]', function (e) {
			let $t = $(this),
				hash = $t.attr('data-scroll');

			if ( $(hash).length > 0){
				e.preventDefault();
				$('html,body').animate({scrollTop:$(hash).offset().top + 1}, 500);
			}
		})
		.on('click', '.form__input-clear', function (e) {
			let $t = $(this),
				$field = $t.closest('.form__field'),
				$input = $field.find('input')
			;
			$input.val('');
			$field.removeClass('f-filled f-error f-message')

		})
		.on('click', '.form__input-arrow', function (e) {
			let $t = $(this),
				$field = $t.closest('.form__field'),
				$input = $field.find('input')
			;
			$input.focus();

		})
		.on('mousedown', '.form__select-item', function (e) {
			let $t = $(this),
				$inputText = $t.closest('.form__field').find('.form__input-text'),
				$inputValue = $t.closest('.form__field').find('.form__input-value'),
				value = $t.attr('data-value'),
				text = $t.html()
			;

			$inputText.val(text);
			$inputValue.val(value);
		})

		.on('blur', '.form__field input, .form__field textarea', function (e) {
			let $input = $(this),
				$field = $input.closest('.form__field');
			$field.removeClass('f-focused');

			if ( !$.trim($input.val()) ){
				$field.removeClass('f-filled');
			} else {
				$field.addClass('f-filled');
			}
		})

		.on('submit', '.form', function (e) {
			let $form = $(this),
				$item = $form.find('input'),
				wrong = false
			;

			$item.each(function () {
				let input = $(this), rule = $(this).attr('data-require');
				$(input)
					.closest('.form__field')
					.removeClass('f-error f-message')
					.find('.form__message')
					.html('')
				;

				if ( rule ){
					form.validate(input[0], rule, err =>{
						if (err.errors.length) {
							wrong = true;
							$(input)
								.closest('.form__field')
								.addClass('f-error f-message')
								.find('.form__message')
								.html(err.errors[0])
							;
						}
					})
				}
			});

			if ( wrong ){
				e.preventDefault();
			}
		})



});

