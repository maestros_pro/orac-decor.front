/**
 *	@param template		string - шаблон выводимого блока (попапа)
 *  @param timeout		boolean - больше не проверять браузер / Number - количество часов, через которое првести следующуб проверку.
 *  @param bodyClass	string / null - класс для body если браузер не проходить проверку.
 *  @param showTemplate	boolean - выводить или не выводить шаблон, если браузер не проходить проверку.
 *  @param close		boolean - добавлять или нет кнопку закрытия шаблона.
 *  @param testTitle	string - текст заголовка в шаблоне по умолчанию.
 *  @param testSubTitle	string - текст подзаголовка в шаблоне по умолчанию.
 *  @param testMessage	string - текст сообщения в шаблоне по умолчанию.
 *  @param check		array - условия проверки браузеров
 * 		1 - browser name (chrome, firefox, opera, opera mini, safari)
 *		2 - browser version
 *		3 - os, platform win/mac (optional)
 *		4 - desktop or mobile view (optional)
 *		example:
 *			['msie', '11', '0', 'desktop'],
 *			['chrome', '42', 'win', 'desktop'],
 *			['firefox', '40'],
 *			['opera', '31'],
 *			['safari', '0', 'win'],
 *			['safari', '8', 'mac']
 *
 */

export default class BrowserDetect{
	constructor(options) {
		this._polyfill();
		this.timeout = 0;
		this.browserInfo = BrowserDetect.userAgentMatch(window.navigator.userAgent);
		this._setOption(options);
		this._init();
	}

	static userAgentMatch(ua) {
		// If an UA is not provided, default to the current browser UA.
		if (ua === undefined) {
			ua = window.navigator.userAgent;
		}
		ua = ua.toLowerCase();

		let match = /(edge)\/([\w.]+)/.exec(ua) ||
			/(vivaldi)[\/]([\w.]+)/.exec(ua) ||
			/(yabrowser)[\/]([\w.]+)/.exec(ua) ||
			/(firefox)[\/]([\w.]+)/.exec(ua) ||
			/(opr)[\/]([\w.]+)/.exec(ua) ||
			/(chrome)[ \/]([\w.]+)/.exec(ua) ||
			/(version)(applewebkit)[ \/]([\w.]+).*(safari)[ \/]([\w.]+)/.exec(ua) ||
			/(webkit)[ \/]([\w.]+).*(version)[ \/]([\w.]+).*(safari)[ \/]([\w.]+)/.exec(ua) ||
			/(webkit)[ \/]([\w.]+)/.exec(ua) ||
			/(opera mini)[ \/]([\w.]+)/.exec(ua) ||
			/(opera)(?:.*version|)[ \/]([\w.]+)/.exec(ua) ||
			/(msie) ([\w.]+)/.exec(ua) ||
			ua.indexOf("trident") >= 0 && /(rv)(?::| )([\w.]+)/.exec(ua) ||
			ua.indexOf("compatible") < 0 && /(mozilla)(?:.*? rv:([\w.]+)|)/.exec(ua) ||
			[];

		let platform_match = /(ipad)/.exec(ua) ||
			/(ipod)/.exec(ua) ||
			/(iphone)/.exec(ua) ||
			/(kindle)/.exec(ua) ||
			/(silk)/.exec(ua) ||
			/(android)/.exec(ua) ||
			/(windows phone)/.exec(ua) ||
			/(win)/.exec(ua) ||
			/(mac)/.exec(ua) ||
			/(linux)/.exec(ua) ||
			/(cros)/.exec(ua) ||
			/(playbook)/.exec(ua) ||
			/(bb)/.exec(ua) ||
			/(blackberry)/.exec(ua) ||
			[];

		let browser = {},
			matched = {
				browser: match[5] || match[3] || match[1] || "",
				version: match[2] || match[4] || "0",
				versionNumber: match[4] || match[2] || "0",
				platform: platform_match[0] || ""
			};

		if (matched.browser) {
			browser[matched.browser] = true;
			browser.version = matched.version;
			browser.versionNumber = parseInt(matched.versionNumber, 10);
		}

		if (matched.platform) {
			browser[matched.platform] = true;
		}

		// These are all considered mobile platforms, meaning they run a mobile browser
		if (browser.android || browser.bb || browser.blackberry || browser.ipad || browser.iphone ||
			browser.ipod || browser.kindle || browser.playbook || browser.silk || browser["opera mini"] || browser["windows phone"]) {
			browser.mobile = true;
			browser.device = 'mobile';
		}

		// These are all considered desktop platforms, meaning they run a desktop browser
		if (browser.cros || browser.mac || browser.linux || browser.win) {
			browser.desktop = true;
			browser.device = 'desktop';
		}

		// Chrome, Opera 15+ and Safari are webkit based browsers
		if (browser.chrome || browser.opr || browser.safari) {
			browser.webkit = true;
		}

		// IE11 has a new token so we will assign it msie to avoid breaking changes
		// IE12 disguises itself as Chrome, but adds a new Edge token.
		if (browser.rv || browser.edge) {
			let ie = "msie";
			matched.browser = ie;
			browser[ie] = true;
		}

		if (browser.edge) {
			let edge = "edge";
			matched.browser = edge;
			browser[edge] = true;
		}

		// Blackberry browsers are marked as Safari on BlackBerry
		if (browser.safari && browser.blackberry) {
			let blackberry = "blackberry";
			matched.browser = blackberry;
			browser[blackberry] = true;
		}

		// Vivaldi browser
		if (browser.vivaldi) {
			matched.browser = "Vivaldi";
		}

		// Yandex browser
		if (browser.yabrowser) {
			matched.browser = "yandex";
		}

		// Playbook browsers are marked as Safari on Playbook
		if (browser.safari && browser.playbook) {
			let playbook = "playbook";
			matched.browser = playbook;
			browser[playbook] = true;
		}

		// BB10 is a newer OS version of BlackBerry
		if (browser.bb) {
			let bb = "blackberry";
			matched.browser = bb;
			browser[bb] = true;
		}

		// Opera 15+ are identified as opr
		if (browser.opr) {
			let opera = "opera";
			matched.browser = opera;
			browser[opera] = true;
		}

		// Stock Android browsers are marked as Safari on Android.
		if (browser.safari && browser.android) {
			let android = "android";
			matched.browser = android;
			browser[android] = true;
		}

		// Kindle browsers are marked as Safari on Kindle
		if (browser.safari && browser.kindle) {
			let kindle = "kindle";
			matched.browser = kindle;
			browser[kindle] = true;
		}

		// Kindle Silk browsers are marked as Safari on Kindle
		if (browser.safari && browser.silk) {
			let silk = "silk";
			matched.browser = silk;
			browser[silk] = true;
		}

		// Assign the name and platform variable
		browser.name = matched.browser;
		browser.platform = matched.platform;

		return browser;
	}

	_polyfill() {

		//assign
		if (!Object.assign) {
			Object.defineProperty(Object, 'assign', {
				enumerable: false,
				configurable: true,
				writable: true,
				value: function (target, firstSource) {
					'use strict';
					if (target === undefined || target === null) {
						throw new TypeError('Cannot convert first argument to object');
					}

					let to = Object(target);
					for (let i = 1; i < arguments.length; i++) {
						let nextSource = arguments[i];
						if (nextSource === undefined || nextSource === null) {
							continue;
						}

						let keysArray = Object.keys(Object(nextSource));
						for (let nextIndex = 0, len = keysArray.length; nextIndex < len; nextIndex++) {
							let nextKey = keysArray[nextIndex],
								desc = Object.getOwnPropertyDescriptor(nextSource, nextKey);
							if (desc !== undefined && desc.enumerable) {
								to[nextKey] = nextSource[nextKey];
							}
						}
					}
					return to;
				}
			});
		}
	}

	_setOption(options) {
		this.config = {
			timeOutLocalStorageName: 'browserCheck',
			check: [
				['edge', '17', false, 'desktop'],
				['msie', '11', false, 'desktop'],
				['chrome', '70', 'win', 'desktop'],
				['firefox', '40'],
				['opera', '31'],
				['safari', false, 'win'],
				['safari', '8', 'mac']
			],
			onInit: function () {
			},
			onCheckFailed: function () {
			}
		};

		Object.assign(this.config, options);
	}

	_browserCheck() {
		let i = 0, checkFailed = false;
		for (i; i < this.config.check.length; i++) {
			let browserName = this.config.check[i][0] == this.browserInfo.name,
				browserVer = this.config.check[i][1] ? ( this.config.check[i][1] > this.browserInfo.versionNumber ) : true,
				browserOs = this.config.check[i][2] ? ( this.config.check[i][2] == this.browserInfo.platform ) : true,
				browserView = this.config.check[i][3] ? ( this.config.check[i][3] == this.browserInfo.device ) : true;

			if (browserName && browserVer && browserOs && browserView) {
				checkFailed = true;
			}
		}

		return checkFailed;
	};

	_supportsLocalStorage() {
		try {
			return 'localStorage' in window && window['localStorage'] !== null;
		} catch (e) {
			return false;
		}
	}

	_init() {

		let check = this._browserCheck(),
			data = {};

		data.checkFailed = check;
		data.deviceType = this.browserInfo.device;
		data.devicePlatform = this.browserInfo.platform;
		data.browserName = this.browserInfo.name;
		data.browserVersionFull = this.browserInfo.version;
		data.browserVersion = this.browserInfo.versionNumber;
		if (this.browserInfo.webkit) data.webkit = this.browserInfo.webkit;
		if (this.getTimeOut()) data.timeout = this.timeout;

		if (!this.timeout && check && typeof this.config.onCheckFailed === 'function') this.config.onCheckFailed(data);

		if (typeof this.config.onInit === 'function') this.config.onInit(data);

		return data;
	};

	setTimeOut(hours) {

		if (this._supportsLocalStorage()) {
			var timeOut = Date.parse(new Date()) + ( hours * 3600000 );
			localStorage.setItem(this.config.timeOutLocalStorageName, timeOut);

			return this.getTimeOut();
		}
	}

	getTimeOut(type) {

		if (this._supportsLocalStorage()) {

			let ls = +localStorage.getItem(this.config.timeOutLocalStorageName);

			this.timeout = ls - Date.parse(new Date());

			if (this.timeout <= 0) {
				this.timeout = 0;
				localStorage.removeItem(this.config.timeOutLocalStorageName);
			}

			switch (type) {
				case 's':
					return this.timeout / 1000;
					break;

				case 'm':
					return this.timeout / 60000;
					break;

				case 'h':
					return this.timeout / 3600000;
					break;

				default:
					return this.timeout;
					break;
			}

		}
	}
}