

export default class ColorPicker {
	constructor(options) {

		this._polyfill();
		
		this.canvas = null;

		Object.assign(this._options = {}, this._default(), options);
		if (document.readyState === 'loading') {
			document.addEventListener('DOMContentLoaded', () => {
				this.init();
			});
		} else {
			this.init();
		}
	}

	_polyfill() {
		if (!Object.assign) {
			Object.defineProperty(Object, 'assign', {
				enumerable: false,
				configurable: true,
				writable: true,
				value: function (target, firstSource) {
					'use strict';
					if (target === undefined || target === null) {
						throw new TypeError('Cannot convert first argument to object');
					}

					let to = Object(target);
					for (let i = 1; i < arguments.length; i++) {
						let nextSource = arguments[i];
						if (nextSource === undefined || nextSource === null) {
							continue;
						}

						let keysArray = Object.keys(Object(nextSource));
						for (let nextIndex = 0, len = keysArray.length; nextIndex < len; nextIndex++) {
							let nextKey = keysArray[nextIndex],
								desc = Object.getOwnPropertyDescriptor(nextSource, nextKey);
							if (desc !== undefined && desc.enumerable) {
								to[nextKey] = nextSource[nextKey];
							}
						}
					}
					return to;
				}
			});
		}


		
	}

	_default() {
		return {
			element: false,
			gradient: [
				'#f30610',
				'#e900ff',
				'#4c00ff',
				'#59ebfe',
				'#2dff00',
				'#f7ff03',
				'#e30a0c'
			]
		}
	}

	_buildControl(){
		let el = document.querySelectorAll(this._options.element)[0];

		this.canvas = document.createElement('canvas');
		this.canvas.width = el.clientWidth;
		this.canvas.height = el.clientHeight;
		el.appendChild(this.canvas);

		this._drawControl();
	}

	_drawControl(){

		this.ctx = this.canvas.getContext('2d');

		let grd = this.ctx.createLinearGradient(0, 0, 0, this.canvas.height);

		this.ctx.clearRect(0, 0, 0, this.canvas.height);


		this.ctx.beginPath();


		for(let i = 0; i < this._options.gradient.length; i++){
			grd.addColorStop((i * (1 / (this._options.gradient.length - 1))), this._options.gradient[i]);
		}

		this.ctx.fillStyle = grd;
		this.ctx.fillRect(0, 0, this.canvas.width, this.canvas.height);

		window.requestAnimationFrame(this._drawControl.bind(this));
	}

	_toHex (color){
		let hex = Number(color).toString(16);
		if (hex.length < 2) {
			hex = "0" + hex;
		}
		return hex;
	}

	_hexColor(r,g,b){
		let red = this._toHex(r),
			green = this._toHex(g),
			blue = this._toHex(b)
		;

		return red + green + blue;
	}

	init(){

		if ( document.querySelectorAll(this._options.element).length ){
			if ( !this._options.element ) {
				console.error('element must be specified');
				return false;
			}
			window.requestAnimationFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame || window.webkitRequestAnimationFrame || window.msRequestAnimationFrame;

			this._buildControl();

			if ( this._options.onPick && typeof this._options.onPick === 'function' ){
				document.querySelectorAll(this._options.element)[0].addEventListener('click', (e)=>{
					let color = this.getColor(e.offsetX, e.offsetY);
					if (color) this._options.onPick(color, e.offsetX, e.offsetY);
				});
			}
			// document.querySelectorAll(this._options.element)[0].addEventListener('mousemove', (e)=>{
			// 	let color = this.ctx.getImageData(e.offsetX, e.offsetY, 1, 1).data;
			// 	if (color[3] !== 0) this.pick = `rgb(${color[0]},${color[1]},${color[2]})`;
			// });
			// document.querySelectorAll(this._options.element)[0].addEventListener('click', (e)=>{
			// 	let color = this.ctx.getImageData(e.offsetX, e.offsetY, 1, 1).data;
			// 	if (color[3] !== 0) this.blend.color = `rgb(${color[0]},${color[1]},${color[2]})`;
			// });
		}


	}


	getColor(x, y){
		let color = this.ctx.getImageData(x, y, 1, 1).data;
		if (color[3] === 0) return false;

		return {
			rgb: `${color[0]},${color[1]},${color[2]}`,
			hex: this._hexColor(color[0],color[1],color[2])
		};
	}

}
